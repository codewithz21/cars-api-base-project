package com.codewithz.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.codewithz.model.Car;

@Repository
public interface CarsRepository extends JpaRepository<Car, Long> {
}
